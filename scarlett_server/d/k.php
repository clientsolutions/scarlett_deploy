<?php
require("config/server_config.php");
require("libraries/sendgrid-php/sendgrid-php.php");

if (isset($_POST['domains']) && ($_POST['domains'] != '')) {
	$domains = $_POST['domains'];
}
else {
	$domains = 'none';
}

if (isset($_POST['subDomains']) && ($_POST['subDomains'] != '')) {
	$subDomains = $_POST['subDomains'];
}
else {
	$subDomains = 'none';
}

if (isset($_POST['jiraTickets']) && ($_POST['jiraTickets'] != '')) {
	$jTix = $_POST['jiraTickets'];
	
	$allTickets = array();
	$totalTickets = (count($jTix))/2;
	
	//echo('total tickets = ' .$totalTickets .'<br />');
	
	for ($i=0; $i<$totalTickets; $i++) {
		$thisTicket;
		
		$thisTicket['title'] = $jTix[0]['value'];
		//echo "setting the title to " .$jTix[0]['value'];
		
		$thisTicket['description'] = $jTix[1]['value'];
		//echo "setting the description to " .$jTix[1]['value'];
		
		$allTickets[] = $thisTicket;
		
		array_shift($jTix);
		array_shift($jTix);
	}
}
else {
	$allTickets = 'none';
}

$allowed_servers  = 'Allowed servers = '; 
//from http://stackoverflow.com/questions/298745/how-do-i-send-a-cross-domain-post-request-via-javascript
foreach ($servers_allowed_to_post as $key => $value) {
		$allowed_servers  .= $value .' , ';
	if ($_SERVER['HTTP_ORIGIN'] == $value) {
		header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
	    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
	    header('Access-Control-Max-Age: 1000');
	    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');		
	}
}

if ($_POST['password'] != md5($server_post_pass)) {
	
	$output = 'Password Incorrect';
}
else {
	
	if ($_POST['deployType'] == 'deployment') {
		/**
		* GIT DEPLOYMENT SCRIPT
		*
		* Used for automatically deploying websites via github or bitbucket, more deets here:
		*
		* https://gist.github.com/1809044
		*/
		 
		// The commands
		$commands = array(
			'echo $PWD',
			'whoami',
			'git fetch --all 2>&1',
			'git reset --hard origin/master 2>&1',
			'git status',
			'git submodule sync',
			'git submodule update',
			'git submodule status'
			
				//how to do it in another directory
				//need to add dirs for www, demo, developer, and scarlett
				//'git --git-dir=/Library/WebServer/Documents/website/newrow_/.git fetch --all 2>&1',
				//'git --git-dir=/Library/WebServer/Documents/website/newrow_/.git reset --hard origin/master 2>&1',
				//'git --git-dir=/Library/WebServer/Documents/website/newrow_/.git status',
				//'git --git-dir=/Library/WebServer/Documents/website/newrow_/.git submodule sync',
				//'git --git-dir=/Library/WebServer/Documents/website/newrow_/.git submodule update',
				//'git --git-dir=/Library/WebServer/Documents/website/newrow_/.git submodule status',
		);
		 
		// Run the commands for output
		$output = '';
		foreach($commands AS $command){
			// Run it
			$tmp = shell_exec($command);
			// Output
			$output .= "<span style='color: #666666;'>\$</span> <span style='color: #000000;'>{$command}</span><br />";
			$output .= htmlentities(trim($tmp)) .'<br />';
		}		
	}
}


$message = '<strong><h3>_Deployment Overview: ' .$_POST['title'] .'</h3></strong>';
$message .= $_POST['desc'] .'<br /><br />';

$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+ <strong>This is a: </strong>' .$_POST['deployType'] .'<br />';
$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+ <strong>This Server: </strong>' .$_SERVER['SERVER_ADDR'] .'<br />';
$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+ <strong>Deployed By: </strong>' .$_POST['email'] .'<br />';
$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+ <strong>Impact: </strong>' .$_POST['impact'] .'<br />';
$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+ <strong>Priority:</strong> ' .$_POST['priority'] .'<br />';
$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+ <strong>Expected Downtime: </strong>' .$_POST['downtime'] .' ' .$_POST['downtime_type'] .'<br /><br />';

$message .= '<strong><h3>_Updated Subdomains: </h3></strong>';
if (is_array($subDomains)) {
	foreach ($subDomains as $key => $value) {
		$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+<strong> ' .$value .'</strong><br />';
	}
}
else {
	$message .= $subDomains;
}
$message .='<br /><br />';

$message .= '<strong><h3>_Affected Domains: </h3></strong>';
if (is_array($domains)) {
	foreach ($domains as $key => $value) {
		$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+<strong> ' .$value .'</strong><br />';	
	}
}
else {
	$message .= $domains;
}
$message .='<br /><br />';

$message .= '<strong><h3>_Jira Tickets Associated: </h3></strong><br />';
if (is_array($allTickets)) {
	foreach ($allTickets as $key => $value) {
		$message .= '&nbsp;&nbsp;&nbsp;&nbsp;+<strong><a href="https://jira.newrow.com/browse/' .$value['title'] .'">' .$value['title'] .'</a></strong> ' .$value['description'] .'<br />';
	}
}
else {
	$message .= $jTix;
}
$message .='<br />';

$message .= '<br />';
$message .= 'there was a new deployment to me @' .$_SERVER['SERVER_ADDR'] .'<br />';
$message .= 'the deployment was requested by ' .$_SERVER['HTTP_ORIGIN'] .'<br />';
$message .= 'the servers allowed to deploy = ' .$allowed_servers .'<br />';
$message .= 'raw output: ' .$output .'<br />';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1';
$output .= 'mail results.....<br />';
$output .= '---------------------------------------------------<br />';

$sendgrid = new SendGrid('nathan@newrow.com', 'yoshimemail00');
$email    = new SendGrid\Email();

foreach ($people_to_email as $key => $emailAddr) {
	$output .= '****************************************<br />';
	$output .='sending mail to ' .$emailAddr .'<br />';
	
	if ($_POST['deployType'] == 'notification') {
		$subject = $_POST['deployServersType'] .' Deployment [Notification]: ' .$_POST['title'];	
    }
	else {
		$subject = $_POST['deployServersType'] .'Deployment [Results]: ' .$_POST['title'];
	}
	
	//sendgrid specific
	$email->addTo($emailAddr)->
       setFrom('scarlett_deploy@newrow.com')->
       setSubject($subject)->
       setText('There was a new deployment to the webserver')->
       setHtml($message);
	   
	//$mail_receipt = mail($emailAddr, $email_subject, $message, $headers);
	
	$output .= '****************************************<br />';
}

$sendgrid->send($email);

header('Content-Type: application/json');
$results['result'] = $output;

echo json_encode($results);
?>