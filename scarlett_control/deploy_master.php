<?php
	require 'config/deploy_config.php';
?>
<html>
	<head>
		<title>deploy to the servers</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
		<!-- http://bootswatch.com/flatly/ -->
		<script language="JavaScript" type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/md5.js"></script>
		<script type="text/javascript">
			var servers;
			var currentServerType;
			
			
			
			function loadServers() {
				console.log('fetching servers');
				$.getJSON( "servers/servers.json", function( data ) {
					console.log("data = " +data);
					console.log("data.Staging = " +data.Staging[0].name)
					servers = data;
					console.log("[after fetch] servers = " + servers);

				$.each(servers, function (key, serverArray){
					
					console.log('we have the server type of ' +key);
					$('#serverGroupSelector')
          				.append($('<option>', { value : key })
          				.text(key)); 
					//set dropdowns here
					
					console.log('set dropdown');
				});

				//this sets the default
				//setList("Staging");

				});
			}
			
			function setList(serverType) {
				console.log('in setList');
				console.log('server type = ' + serverType);
				console.log('servers = ' + servers);
				
				if (serverType == 'optionSelect') {
					//get the selected set
					serverType = $('#serverGroupSelector').val();
					console.log('setting the server type from the selector');
					console.log('serverGroupSelector value = ' + serverType);
					console.log('out of if');
				}
				
				currentServerType = serverType;
				
				if (serverType == 'Production') {
					//set the panel to oh shit color
					$('#server_panel_holder').removeClass('panel-success');
					$('#server_panel_holder').addClass('panel-danger');
				}
				else {
					$('#server_panel_holder').removeClass('panel-danger');
					$('#server_panel_holder').addClass('panel-success');
				}
				
				$('#server_panel_title').html(serverType);
				
				var content = "";
				$.each(servers, function (key, serverArray){
					if (key == serverType) {
						console.log('we have the server type of ' +key);
						var arrayLength = serverArray.length;
						for (var i=0; i < arrayLength; i++) {
							console.log('name = ' + serverArray[i].name);
							content +=  '<div id="' +serverType+ '_' +i +'"><span id="status_' +serverType+ '_' +i +'" class="label">Ready</span> ' +serverArray[i].name + " (" +serverArray[i].address +")</div>";
						}
					}
				});
				
				console.log('content for replacement = ' +content);
				$("#serversList").html(content);
			}
			
			function deploy(notification) {
				
				notification = (typeof notification === "undefined") ? false : notification;
				//make sure form fields are filled
				var canPost = true;
				var fields = Array($('#pss'), $('#email'), $('#desc'));
				
				for (var i=0; i<fields.length; i++) {
					console.log('field val = ' +fields[i].val());
					console.log('field val blank string = ' +(fields[i].val() == ''));
					console.log('field val undefined = ' +(fields[i].val() == undefined));
					
					if (fields[i].val() == '' || fields[i].val() == undefined) {
						fields[i].parent().addClass('has-error');
						canPost = false;
					}
				}
				
				if (canPost) {
					
					//disable the form
					fields.push($('#submitButton'));
					fields.push($('#serverGroupSelector'));
					fields.push($('#domains'));
					fields.push($('#subdomains'));
					fields.push($('#downtime_number'));
					fields.push($('#downtime_timeType'));
					fields.push($('#deploy_impact'));
					fields.push($('#priority'));
					fields.push($('.JiraTicketRepeat input'));
					
					if(!notification) {
						for (var i=0; i<fields.length; i++) {
							//fields[i].prop( "disabled", true );	
						}
						var deployType = 'deployment';
					}
					else{
						var deployType = 'notification';
						console.log('*************should only be notifying*************'); 
					}
					
					console.log('doing the deed');
					console.log('current server type = ' +currentServerType);
					console.log('servers[currentServerType].length = ' + servers[currentServerType].length);
					
					var pass = $('#pss').val();
					console.log('pass value = ' +pass);
					
					var securePass = calcMD5(pass);
					console.log('encrypted MD5 pass value = ' +securePass);
					var deployTitle = $('#deployTitle').val();
					var deployDesc = $('#desc').val();
					var deployServersType = currentServerType;
					var deployEmail = $('#email').val();
					var deploy_impact = $('#deploy_impact').val();
					var priority = $('#priority').val();
					var downtime_number = $('#downtime_number').val();
					var downtime_timeType = $('#downtime_timeType').val();
					var subDomains = $('#subdomains').val();
					var domains = $('#domains').val();
					
					var tix = $('.JiraTicketRepeat input').serializeArray();
					
					//var tixArray = [];
					
					console.log('stop');
					
					//loop through all of the servers in the list
					for (var i=0; i<servers[currentServerType].length; i++) {
						var address = 'http://' +servers[currentServerType][i].address +'/scarlett_server/d/k.php';
						console.log('POSTING TO: server address = ' +address);
						//thank you stack overflow. again.
						//http://stackoverflow.com/questions/2568966/how-do-i-pass-the-value-not-the-reference-of-a-js-variable-to-a-function
						(function (i) {
							var poster = $.post( address, { 
									password: securePass, 
									email: deployEmail,
									title: deployTitle,
									desc: deployDesc,
									impact:deploy_impact,
									priority:priority,
									downtime:downtime_number,
									downtime_type: downtime_timeType,
									subDomains: subDomains,
									domains: domains,
									jiraTickets: tix,
									deployType: deployType,
									deployServersType: deployServersType
								});
							poster.dataType = "json";
							poster.done(function(data) {
								
									var currentServer = i;
									var thisResultsHolderDiv = '#' +currentServerType +'_' +currentServer;
									var thisStatusPil = '#status_' +currentServerType +'_' +currentServer;
									var response = data;
									
									for (var foo in data) {
										console.log('var ' +foo +' = ' +data[foo]);
									}
									
									console.log('putting the results here: ' +thisResultsHolderDiv);
									$(thisResultsHolderDiv).append('<br /><br /><div style="font-size:smaller;" class="alert alert-dismissable alert-success">' +data.result +'</div>');
									$(thisStatusPil).addClass('label-success');
									$(thisStatusPil).html('Success');
									console.log('POSTING DONE, RESPONSE = ' +response);
							});
						})(i);
					}
					//print the results to the page
				}
				else {
					alert('please fix errors');
				}
			}
			
			$(function() {
				console.log( "ready!" );
				
				$('#deploy_form').submit(function(e) {
    				e.preventDefault();
    				return false;
				});
				
				loadServers();
				
				// Add a new repeating section
				//from: http://stackoverflow.com/questions/11380045/repeating-div-with-form-fields
				$('#addTicketButton').click(function(){
				    var currentCount =  $('.JiraTicketRepeat').length;
				    console.log('current items in the div = ' +currentCount);
				    
				    var newCount = currentCount+1;
				    var lastRepeatingGroup = $('.JiraTicketRepeat').last();
				    var newSection = lastRepeatingGroup.clone();
				    newSection.insertAfter(lastRepeatingGroup);
				    newSection.find("input").each(function (index, input) {
				        input.id = input.id.replace("_" + currentCount, "_" + newCount);
				        input.name = input.name.replace("_" + currentCount, "_" + newCount);
				    });
				    newSection.find("label").each(function (index, label) {
				        var l = $(label);
				        l.attr('for', l.attr('for').replace("_" + currentCount, "_" + newCount));
				    });
				    return false;
				});
			});
		</script>
		
	</head>
	<body>
		<div class="jumbotron">
                <h1>DEPLOY</h1>
                <p>Use this to make new rows.</p>
              </div>
		<div class="col-lg-6">
			<div class="well bs-component">
				<form id="deploy_form" class="form-horizontal">
					<fieldset>
						<div class="form-group">
							<label for="select" class="col-lg-2 control-label">Server Group:</label>
							<div class="col-lg-10">
						  	<select class="form-control" id="serverGroupSelector" onchange="setList('optionSelect');">
								<option>Please Select</option>
							</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="email">Email: </label>
							<div class="col-lg-10">
                      		<input type="text" name="email" id="email" class="form-control" />
                      		<span class="help-block">Please enter your email</span>
                    		</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="email">Deployment Title: </label>
							<div class="col-lg-10">
                      		<input type="text" name="deployTitle" id="deployTitle" class="form-control" />
                      		<span class="help-block">Please enter a title for this deployment</span>
                    	</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="desc">Deploy Description: </label>
							<div class="col-lg-10">
								<textarea rows="4" cols="50" name="desc" id="desc" class="form-control" /></textarea>
								<span class="help-block">Please enter a description of this deployment</span>
							</div>	
						</div>
						<hr />
						<h3>Jira Tickets Associated With This Deployment</h3>
						<div class="JiraTicketRepeat">
							<div class="form-group">
								<label class="col-lg-2 control-label" for="jira_ticket_1">Ticket: </label>
								<div class="col-lg-3">
									<input type="text" name="jira_ticket_1" id="jira_ticket_1" class="form-control" placeholder="Jira Ticket ID" />
									<span class="help-block">Jira Ticket ID</span>
								</div>
								<div class="col-lg-5">
									<input type="text" name="jira_desc_1" id="jira_desc_1" class="form-control" placeholder="Jira Ticket Description" />
									<span class="help-block">Jira Ticket Description</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-9">
								&nbsp;
							</div>
							<div class="col-lg-2">
								<button id="addTicketButton" class="btn btn-primary">+</button>
							</div>
						</div>
						<br />
						<hr />
						<div class="form-group">
							<label class="col-lg-2 control-label" for="deploy_impact">Impact: </label>
							<div class="col-lg-10">
							<select class="form-control" id="deploy_impact" name="deploy_impact" onchange="">
								<option>Please Select</option>
								<option value="low">Low</option>
								<option value="medium">Medium</option>
								<option value="high">High</option>
							</select>
								<span class="help-block">Please set the expected impact of this deployment</span>
							</div>	
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="desc">Priority: </label>
							<div class="col-lg-10">
							<select class="form-control" id="priority" name="priority" onchange="">
								<option>Please Select</option>
								<option value="low">Low</option>
								<option value="medium">Medium</option>
								<option value="high">High</option>
								<option value="urgent">Urgent</option>
							</select>
								<span class="help-block">Please set the priority of this deployment</span>
							</div>	
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="downtime_number">Expected Downtime: </label>
							<div class="col-lg-10">
							<select class="form-control" id="downtime_number" name="downtime_number" onchange="">
								<option>Please Select</option>
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="30">30</option>
							</select>
							<select class="form-control" id="downtime_timeType" name="downtime_timeType" onchange="">
								<option>Please Select</option>
								<option value="Seconds">Seconds</option>
								<option value="Minutes">Minutes</option>
								<option value="Hours">Hours</option>
								<option value="Days">Days</option>
							</select>
								<span class="help-block">Please set the priority of this deployment</span>
							</div>	
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="subdomains">Updated Subdomains: </label>
							<div class="col-lg-10">
								<select multiple="" id="subdomains" name="subdomains" class="form-control">
									<option value="www">www</option>
									<option value="demo">demo</option>
									<option value="developer">developer</option>
								</select>
                      		<span class="help-block">Please enter the subdomains that will be affected by this deployment</span>
                    		</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="domains">Affected Domains: </label>
							<div class="col-lg-10">
								<select multiple="" id="domains" name="domains" class="form-control">
									<option value=".com">.com</option>
									<option value=".co.il">.co.il</option>
									<option value=".uk.com">.uk.com</option>
									<option value=".co.za">.co.za</option>
									<option value=".jp">.jp</option>
								</select>
                      		<span class="help-block">Please enter the domains that will be affected by this deployment</span>
                    		</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label" for="pss">Deploy pass: </label>
							<div class="col-lg-10">
                      		<input type="password" name="pss" id="pss" class="form-control" placeholder="Shhhh..." />
                      		<span class="help-block">Please enter the deployment password</span>
                    		</div>
						</div>
						<button type="submit" id="submitButton" onclick="deploy(true);" class="btn btn-primary">Notify</button>
						<button type="submit" id="submitButton" onclick="deploy();" class="btn btn-primary">Deploy</button>
					</fieldset>
				</form>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="well bs-component">
				<br />
                <div id="server_panel_holder" class="panel">
                	<div class="panel-heading">
                  		<h3 id="server_panel_title" class="panel-title">Please choose your servers</h3>
                	</div>
                	<div class="panel-body">
                  		<div id="serversList">this is where the list of servers will go</div>
                	</div>
              </div>
			</div>
		</div>
	</body>
</html>